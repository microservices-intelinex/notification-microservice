import { Controller, Get, Logger } from '@nestjs/common';
import { NotifierService } from './notifier.service';
import { EventPattern, Payload } from '@nestjs/microservices';
import { USER_CREATED } from './config/events';
import { Channel, PayloadNotification, Type } from './app.interface';

@Controller()
export class AppController {
  constructor(private readonly notifierService: NotifierService) {}

  @EventPattern(USER_CREATED)
  notifier(@Payload() data: PayloadNotification): void {
    Logger.log('Sending email notification');
    if (data.channel.includes(Channel.EMAIL)) {
      if (data.type.includes(Type.WELCOM)) {
        this.welcomeNotification(data.name, data.email);
        return;
      }
      return this.otherNotification(data.name, data.email);
    }
    Logger.log('This other type of notification will comming soon');
  }
  private async welcomeNotification(name, email) {
    await this.notifierService.notify(
      email,
      'Hello ' +
        name +
        ' this is a welcome email to the platform intellinex. You can know more in intelinex.com',
      'Welcome email to subscrite at IntellInex',
    );
  }
  private otherNotification(name, email) {
    Logger.log('This need be implemented soon');
  }
}
