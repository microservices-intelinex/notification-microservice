export enum Channel {
  EMAIL = 'EMAIL',
  SMS = 'SMS',
}

export enum Type {
  WELCOM = 'WELCOM',
  NOTIFICATION = 'NOTIFICATION',
}
export interface PayloadNotification {
  email: string;
  channel: Channel;
  type: Type;
  name: string;
}
