import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { NotifierService } from './notifier.service';

import enviroments from './config/enviroments';
import app, { microserviceConfig } from './config/app';
import validation from './config/validation';
import { ConfigModule } from '@nestjs/config';

const ConfigModuleProvider = ConfigModule.forRoot({
  envFilePath: enviroments[process.env.NODE_ENV] || '.env',
  isGlobal: true,
  load: [app, microserviceConfig],
  validationSchema: validation,
});
@Module({
  imports: [ConfigModuleProvider],
  controllers: [AppController],
  providers: [NotifierService],
})
export class AppModule {}
