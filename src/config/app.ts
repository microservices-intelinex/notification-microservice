import { registerAs, ConfigService } from '@nestjs/config';
import { Transport } from '@nestjs/microservices';
import { APP_CONFIG, KAFKA_ID, MICROSERVICE_CONFIG } from './constants';

interface IEnvAppConfig {
  httpPort: number;
}

export type IAppConfig = IEnvAppConfig & ConfigService;

export default registerAs(APP_CONFIG, () => ({
  httpPort: process.env.HTTP_PORT || 3000,
}));

export const microserviceConfig = registerAs(MICROSERVICE_CONFIG, () => ({
  transport: Transport.KAFKA,
  subscribe: {
    fromBeginning: true,
  },
  name: KAFKA_ID,
  options: {
    client: {
      clientId: process.env.MICROSERVICE_CLIENT_ID,
      brokers: [process.env.MICROSERVICE_BROKER],
      ssl: true,
      sasl: {
        mechanism: process.env.MICROSERVICE_MECHANISM,
        username: process.env.MICROSERVICE_USERNAME,
        password: process.env.MICROSERVICE_PASSWORD,
      },
    },
    consumer: {
      groupId: process.env.MICROSERVICE_CONSUMER_GROUP_ID,
    },
  },
}));
