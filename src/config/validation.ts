import Joi = require('joi');
export default Joi.object({
  NODE_ENV: Joi.string().valid('dev', 'stag', 'prod').default('dev'),
  HTTP_PORT: Joi.number().required(),
});
