import * as dotenv from 'dotenv';
dotenv.config();
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { APP_CONFIG, MICROSERVICE_CONFIG } from './config/constants';
import { Logger } from '@nestjs/common';
import { IAppConfig } from './config/app';
import { ConfigService } from '@nestjs/config';
async function bootstrap() {
  Logger.log('APP NOTIFICATIONS STATING');
  const app = await NestFactory.create(AppModule);
  const config = app.get<IAppConfig>(ConfigService);
  app.connectMicroservice(config.get(MICROSERVICE_CONFIG));
  app.startAllMicroservices();

  await app.listen(config.get<IAppConfig>(APP_CONFIG).httpPort);
}
bootstrap();
